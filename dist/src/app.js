"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _express = require('express'); var _express2 = _interopRequireDefault(_express);
var _cors = require('cors'); var _cors2 = _interopRequireDefault(_cors);
var _dotenv = require('dotenv'); var _dotenv2 = _interopRequireDefault(_dotenv);
var _path = require('path'); var _path2 = _interopRequireDefault(_path);
var _expressejslayouts = require('express-ejs-layouts'); var _expressejslayouts2 = _interopRequireDefault(_expressejslayouts);
var _consign = require('consign'); var _consign2 = _interopRequireDefault(_consign);

// import routes from './routes'

class App {
    
     __init() {this.localPath = _path2.default.join(__dirname)}

     constructor () {;App.prototype.__init.call(this);
      _dotenv2.default.config()

      this.express = _express2.default.call(void 0, )

      this.middlewares()
      this.layout()
      this.routes()
    }

     middlewares () {
      this.express.use(_express2.default.urlencoded())
      this.express.use(_express2.default.json())
      this.express.use(_cors2.default.call(void 0, ))
    }

     layout () {
      this.express.set('views', this.localPath + '/views')
      this.express.set('view engine', 'ejs')
      this.express.use(_expressejslayouts2.default)
      this.express.use(_express2.default.static(this.localPath + '/public'))
    }

     routes () {
      _consign2.default.call(void 0, {
        cwd: 'src'
      }).include('controllers').into(this.express)
    }
}

exports. default = new App().express
