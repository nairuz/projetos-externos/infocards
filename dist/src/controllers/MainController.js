"use strict";

class MainController {
   async home (req, res) {
    return res.render('pages/index', {})
  }

   async about (req, res) {
    return res.render('pages/about', {})
  }  

   async products (req, res) {
    return res.render('pages/products', {})
  }  
  
   async services (req, res) {
    return res.render('pages/services', {})
  } 

   async technology (req, res) {
    return res.render('pages/technology', {})
  } 

   async customers (req, res) {
    return res.render('pages/customers', {})
  } 

   async contact (req, res) {
    return res.render('pages/contact', {})
  } 

   async notfound (req, res) {
    return res.render('pages/notfound', {})
  } 


}

module.exports = (app) => {
  const main = new MainController()

  app.get('/', main.home)
  app.get('/sobre', main.about)
  app.get('/produtos', main.products)
  app.get('/servicos', main.services)
  app.get('/tecnologia', main.technology)
  app.get('/clientes', main.customers)
  app.get('/contato', main.contact)
  app.get('/404', main.notfound)
  app.use(function (req, res, next) {
    res.status(404).render('pages/notfound', {})
  })
}
