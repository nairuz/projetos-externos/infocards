"use strict";Object.defineProperty(exports, "__esModule", {value: true});
 class Helpers {
  static suffler (a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]]
    }
    return a
  }
} exports.default = Helpers;
