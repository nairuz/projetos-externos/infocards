// Carrossel de Clientes - Home

$('.carrossel-clientes').owlCarousel({
      loop: true,
      navigation: true,
      navigationText: ["<i class='fa fa-arrow-left'></i>", "<i class='fa fa-arrow-right'></i>"],
      items: 2,
      itemsDesktop: [1199, 2],
      itemsDesktopSmall: [979, 2],
      itemsTablet: [768, 1],
      itemsMobile: [450, 1]
});

// Efeito Hover - Serviços
jQuery(function(){
      jQuery(".card-servico").hover(
          function(){
              jQuery(this).addClass('hoverOn');    
          },
          function(){
              jQuery(this).removeClass('hoverOn');
          }
      );
  });

//   Alteração de posição - Imagem Cartões Besni 

  jQuery(document).ready(function() {
    if( jQuery(window).width() <= 600){
        jQuery('.besni .img-clientes-cases').appendTo('.besni');
    }
});

// Menu Lateral Mobile
$( "#icon-menu-lateral" ).click(function() {
    $('#menu-lateral').css('right','0');
    $('#fade-menu').css('display','block');
  });

  $( ".coluna2-nz .fa" ).click(function() {
    $('#menu-lateral').css('right','-295px');
    $('#fade-menu').css('display','none');
  });

  $( "#fade-menu" ).click(function() {
    $('#menu-lateral').css('right','-295px');
    $('#fade-menu').css('display','none');
  });
  