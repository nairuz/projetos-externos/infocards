import { Request, Response, Application } from 'express'

class MainController {
  public async home (req: Request, res: Response): Promise<any> {
    return res.render('pages/index', {})
  }

  public async about (req: Request, res: Response): Promise<any> {
    return res.render('pages/about', {})
  }  

  public async products (req: Request, res: Response): Promise<any> {
    return res.render('pages/products', {})
  }  
  
  public async services (req: Request, res: Response): Promise<any> {
    return res.render('pages/services', {})
  } 

  public async technology (req: Request, res: Response): Promise<any> {
    return res.render('pages/technology', {})
  } 

  public async customers (req: Request, res: Response): Promise<any> {
    return res.render('pages/customers', {})
  } 

  public async contact (req: Request, res: Response): Promise<any> {
    return res.render('pages/contact', {})
  } 

  public async notfound (req: Request, res: Response): Promise<any> {
    return res.render('pages/notfound', {})
  } 


}

export = (app: Application):void => {
  const main = new MainController()

  app.get('/', main.home)
  app.get('/sobre', main.about)
  app.get('/produtos', main.products)
  app.get('/servicos', main.services)
  app.get('/tecnologia', main.technology)
  app.get('/clientes', main.customers)
  app.get('/contato', main.contact)
  app.get('/404', main.notfound)
  app.use(function (req, res, next) {
    res.status(404).render('pages/notfound', {})
  })
}
