import { Request, Response, Application } from 'express'
import nodemailer from "nodemailer";

class ContactController {

  public async contact (req: Request, res: Response): Promise<any> {
    return res.render('pages/contact', {})
  } 

  public async sendEmail( req: Request, res: Response): Promise<any> {
        const emailFrom = 'comercial@infocards.com.br'
        const passwordFrom = 'Todos!@#'
        const host = 'smtp.infocards.com.br'
        const emailTo = 'comercial@infocards.com.br'

        // // pode usar e-mail monitor@creditsender.net.br , senha Todos!@#, configurar o smtp como smtp.infocards.com.br.
        let transporter = nodemailer.createTransport({
            host: host,
            port: 465,
            secure: true, 
            auth: {
                user: emailFrom, 
                pass: passwordFrom,
            },
        });
    
        const mailOptions = {
            from: emailFrom, 
            to: emailTo, 
            subject: `Nova Mensagem Formulário de Contato - Infocards `,
            html: `Nome: ${req.body.nome} <br> E-mail: ${req.body.email} <br> Telefone: ${req.body.telefone} <br> Mensagem: ${req.body.text}`,
        }

        await transporter.sendMail(mailOptions, (err, info) => { // Função que, efetivamente, envia o email.
            if (err) {
              return res.status(500).json({'status': 'error'})
            }
            return res.status(200).json({status: 'sucesso'})
        })
  }
  
}

export = (app: Application):void => {
  const main = new ContactController()
  app.get('/contato', main.contact)
  app.post('/contato/enviar', main.sendEmail)
}
