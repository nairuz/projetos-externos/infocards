import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import path from 'path'
import expressLayouts from 'express-ejs-layouts'
import consign from 'consign'

// import routes from './routes'

class App {
    public express: express.Application
    private localPath = path.join(__dirname)

    public constructor () {
      dotenv.config()

      this.express = express()

      this.middlewares()
      this.layout()
      this.routes()
    }

    private middlewares (): void {
      this.express.use(express.urlencoded())
      this.express.use(express.json())
      this.express.use(cors())
    }

    private layout (): void {
      this.express.set('views', this.localPath + '/views')
      this.express.set('view engine', 'ejs')
      this.express.use(expressLayouts)
      this.express.use(express.static(this.localPath + '/public'))
    }

    private routes (): void {
      consign({
        cwd: 'src'
      }).include('controllers').into(this.express)
    }
}

export default new App().express
